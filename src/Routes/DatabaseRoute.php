<?php

namespace Logger\Routes;

use PDO;
use Logger\Route;

/**
 * Class DatabaseRouter
 *
 * CREATE TABLE table_log (
 *      id integer PRIMARY KEY,
 *      date date,
 *      level varchar(100),
 *      message text,
 *      context text
 * );
 */
class DatabaseRoute extends Route
{

    /**
     * connect to dateBase
     */
    public $dsn;
    public $username;
    public $password;
    public $table;
    private $connection;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->connection = new PDO($this->dsn, $this->username, $this->password);
    }

    public function log($level, $message, array $context = [])
    {
        // не успел :(
    }
}
