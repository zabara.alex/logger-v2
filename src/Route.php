<?php

namespace Logger;

use DateTime;
use ReflectionClass;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * Class Route
 */
abstract class Route extends AbstractLogger implements LoggerInterface
{

    public $enabled = true;
    public $dateFormat = 'D-M-Y H:i:s';

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $reflection = new ReflectionClass($this);
        foreach ($attributes as $attribute => $value) {
            $property = $reflection->getProperty($attribute);
            if ($property->isPublic()) {
                $property->setValue($this, $value);
            }
        }
    }

    /**
     * To day date and time
     *
     * @return string
     */
    protected function getDate()
    {
        return (new DateTime())->format($this->dateFormat);
    }

    /**
     * Format $context to string
     *
     * @param array $context
     *
     * @return string
     */
    protected function contextStringify(array $context = [])
    {
        return !empty($context) ? json_encode($context) : null;
    }
}