<?php

namespace Logger;

use Iterator;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * Class Logger
 */
class Logger extends AbstractLogger implements LoggerInterface
{
    private $routes;

    /**
     * This is construct
     *
     * @param Iterator $routes
     */
    public function __construct(Iterator $routes)
    {
        $this->routes = $routes;
    }

    /**
     * This is log function
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void|null
     */
    public function log($level, $message, array $context = [])
    {
        foreach ($this->routes as $route) {
            if (!$route instanceof Route) {
                continue;
            }
            if (!$route->enabled) {
                continue;
            }

            $route->log($level, $message, $context);
        }
    }
}