<?php
include 'vendor/autoload.php';

$routes = new SplObjectStorage();

$routes->attach(new Logger\Routes\FileRoute([
	'enabled' => true,
	'filePath' => 'log.log',
]));

//$routes->attach(new Logger\Routes\DatabaseRoute([
//	'enabled' => true,
//	'dsn' => '__________',
//	'table' => 'table_log',
//]));

$log = new Logger\Logger($routes);

$log->info('Info log');
$log->alert('Alert log');
$log->error('Error log');
$log->debug('Debug log');
$log->notice('Notice log');
$log->warning('Warning log');
$log->critical('Critical log');
$log->emergency('Emergency log');